/*
 * Maven Release Helper
 * ----------------------------------------------------------------------------
 * This is a simple tool to help with performing the same actions that the 
 * maven release plugin does but does it in a more... less... automatic way.
 * 
 * This helps mostly when the maven project in question does not follow the 
 * maven way perfectly or you just want more control on what needs to happen.
 *
 * ----------------------------------------------------------------------------
 * 
 * Author:
 * Aditya Gaddam <lostinspacebar.com>
 *
 * License:
 * Do whatever you want. Have fun.
 */
package com.lostinspacebar.mavenreleasehelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNCommitClient;
import org.tmatesoft.svn.core.wc.SVNCopyClient;
import org.tmatesoft.svn.core.wc.SVNCopySource;
import org.tmatesoft.svn.core.wc.SVNInfo;
import org.tmatesoft.svn.core.wc.SVNRevision;

/**
 * Driver for the maven release helper.
 * 
 * @revision $rev: $
 * @author Aditya Gaddam <www.lostinspacebar.com>
 */
public class Main {

    private static final Logger logger = Logger.getLogger("ReleaseHelper");
    private static SVNClientManager svnClient;
    private static String jdkHome;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SVNException, IOException, InterruptedException, ParseException, Exception {
        
        System.setProperty("java.util.logging.SimpleFormatter.format",
                    "%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS.%1$tL %4$-7s %5$s %6$s%n");
        
        logger.info("Maven Release Helper Version : $rev: $");
        logger.info("------------------------------------------------------");
        
        CommandLineParser parser = new BasicParser();
        Options options = new Options();
        options.addOption("project", true, "Main project file.");
        options.addOption("baseDir", true, "Base directory. Used to know what the root for the tag is.");
        options.addOption("oldVersion", true, "Version to move from.");
        options.addOption("newVersion", true, "Release version to move to.");
        options.addOption("newDevelopmentVersion", true, "Development version to move to.");
        options.addOption("authFile", true, "Authentication for SCM");
        options.addOption("tagBaseDir", true, "Base directory for tags. If left alone, /tags is used.");
        options.addOption("tagPrefix", true, "Tag prefix.");
        options.addOption("jdkHome", true, "JDK Home");

        Option propOption = new Option("property", true, "Specifies version moves for properties.");
        propOption.setArgs(Option.UNLIMITED_VALUES);
        options.addOption(propOption);

        Option packageOption = new Option("package", true, "Specifies version moves for packages.");
        packageOption.setArgs(Option.UNLIMITED_VALUES);
        options.addOption(packageOption);

        CommandLine cmd = parser.parse(options, args);

        String project = cmd.getOptionValue("project");
        String baseDir = cmd.getOptionValue("baseDir");
        String oldVersion = cmd.getOptionValue("oldVersion");
        String newVersion = cmd.getOptionValue("newVersion");
        String newDevelopmentVersion = cmd.getOptionValue("newDevelopmentVersion");
        String authFile = cmd.getOptionValue("authFile");
        String tagBaseDir = cmd.getOptionValue("tagBaseDir");
        String tagPrefix = cmd.getOptionValue("tagPrefix");
        jdkHome = cmd.getOptionValue("jdkHome");
        String[] properties = cmd.getOptionValues("property");
        String[] packages = cmd.getOptionValues("package");

        String authFileContent = new String(Files.readAllBytes(new File(authFile).toPath()));
        String[] authParts = authFileContent.trim().split(",");
        String username = authParts[0];
        String password = authParts[1];
        
        logger.info(String.format("Project = %s", project));
        logger.info(String.format("Base Directory = %s", baseDir));
        logger.info(String.format("Current Development Version = %s", oldVersion));
        logger.info(String.format("Release Version = %s", newVersion));
        logger.info(String.format("New Development Version = %s", newDevelopmentVersion));
        logger.info(String.format("Tag Base Directory = %s", tagBaseDir));
        logger.info(String.format("Tag Prefix = %s", tagPrefix));

        logger.info("Property Overrides");
        List<VersionMoveDefinition> propertyDefinitions = new ArrayList<>();
        if (properties != null) {
            for (String propertyDef : properties) {
                VersionMoveDefinition def = new VersionMoveDefinition(propertyDef);
                logger.info(String.format(" - Name [%s]: Versions [%s] -> [%s] -> [%s]", 
                        def.getName(), def.getOldVersion(), def.getNewVersion(), def.getNewDevelopmentVersion()));
                propertyDefinitions.add(def);
            }
        }

        logger.info("Package Overrides");
        List<VersionMoveDefinition> packageDefinitions = new ArrayList<>();
        if (packages != null) {
            for (String packageDef : packages) {
                VersionMoveDefinition def = new VersionMoveDefinition(packageDef);
                logger.info(String.format(" - Name [%s]: Versions [%s] -> [%s] -> [%s]", 
                        def.getName(), def.getOldVersion(), def.getNewVersion(), def.getNewDevelopmentVersion()));
                packageDefinitions.add(def);
            }
        }

        // Create SVN client
        svnClient = SVNClientManager.newInstance(new DefaultSVNOptions(), username, password);

        // Figure out remote URL
        SVNURL url = svnClient.getWCClient().doInfo(new File(baseDir), SVNRevision.HEAD).getRepositoryRootURL();
        url = url.appendPath("tags", true);
        logger.info(String.format("Remote URL = %s", url.toString()));

        // If tagBase isn't specified, just use root + tags
        if (tagBaseDir == null) {
            tagBaseDir = "tags";
        }

        // Build current snapshot version to make sure build is stable
        logger.info(String.format("Build current snapshot version %s to verify validity.", oldVersion));
        buildProject(project);

        // Move project to release
        logger.info(String.format("Moving version from development %s to release %s.", oldVersion, newVersion));
        moveVersion(project, new VersionMoveDefinition("project", oldVersion, newVersion, newDevelopmentVersion), true,
                propertyDefinitions.toArray(new VersionMoveDefinition[propertyDefinitions.size()]),
                packageDefinitions.toArray(new VersionMoveDefinition[packageDefinitions.size()]));

        // Build to make sure nothing broke
        logger.info(String.format("Building release project version %s to verify validity.", newVersion));
        buildProject(project);

        // Commit changes
        logger.info(String.format("Committing released project %s with version %s", baseDir, newVersion));
        commit(baseDir, String.format("Commiting version change from %s%s (development) -> %s%s (release)", tagPrefix, oldVersion, tagPrefix, newVersion));

        // Tag the head 
        logger.info(String.format("Tagging release version %s", newVersion));
        tagRevision(baseDir, SVNRevision.HEAD, tagBaseDir, tagPrefix + newVersion, String.format("Tagging release revision %s", newVersion));

        // Move version to new development version
        logger.info(String.format("Switching to new development version %s", newDevelopmentVersion));
        moveVersion(project, new VersionMoveDefinition("project", oldVersion, newVersion, newDevelopmentVersion), false,
                propertyDefinitions.toArray(new VersionMoveDefinition[propertyDefinitions.size()]),
                packageDefinitions.toArray(new VersionMoveDefinition[packageDefinitions.size()]));

        // Build to make sure nothing broke
        logger.info(String.format("Building new snapshot project version %s to verify validity.", newDevelopmentVersion));
        buildProject(project);

        // Commit new version
        logger.info(String.format("Commiting new development version of project %s with version %s", baseDir, newDevelopmentVersion));
        commit(baseDir, String.format("Commiting version change from %s%s (release) -> %s%s (development)", tagPrefix, newVersion, tagPrefix, newDevelopmentVersion));
    }

    private static long commit(String baseDir, String message) throws SVNException {
        SVNCommitClient commitClient = svnClient.getCommitClient();
        SVNCommitInfo result = commitClient.doCommit(new File[]{new File(baseDir)}, false, message, null, null, false, true, SVNDepth.INFINITY);
        return result.getNewRevision();
    }

    private static void tagRevision(String baseDir, SVNRevision revision, String tagBaseDir, String tag, String message) throws SVNException {

        // Destination tag Url
        File wcFile = new File(baseDir);
        SVNInfo wcInfo = svnClient.getWCClient().doInfo(wcFile, revision);
        SVNURL tagUrl = wcInfo.getRepositoryRootURL().appendPath(tagBaseDir, true).appendPath(tag, true);

        // Source working copy folder
        SVNCopySource source = new SVNCopySource(revision, revision, wcFile);

        // Copy
        SVNCopyClient copyClient = svnClient.getCopyClient();
        SVNCommitInfo commitInfo = copyClient.doCopy(new SVNCopySource[]{source}, tagUrl, false, true, true, message, new SVNProperties());
    }

    private static void buildProject(String path) throws IOException, InterruptedException, Exception {
        String line;
        ProcessBuilder pb = new ProcessBuilder(new String[]{
            "cmd",
            "/c",
            "mvn -B",
            "clean",
            "deploy",
            "-f",
            path
        });
        Map<String, String> env = pb.environment();
        env.put("JAVA_HOME", jdkHome);

        Process p = pb.start();
        try (BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
             BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()))) {

            while ((line = bri.readLine()) != null) {
                System.out.println(line);
            }

            while ((line = bre.readLine()) != null) {
                System.out.println(line);
            }

        }

        p.waitFor();
        System.out.println("Done. Exit Value = " + p.exitValue());

        if (p.exitValue() > 0) {
            throw new Exception(String.format("Build failed for project: %s\n", path));
        }
    }

    private static String getCoordinates(Parent parent) {
        return parent.getGroupId() + ":" + parent.getArtifactId();
    }

    private static String getCoordinates(Dependency dependency) {
        String groupId = dependency.getGroupId();
        String artifactId = dependency.getArtifactId();

        return groupId + ":" + artifactId;
    }

    private static String getCoordinates(Model model) {
        String groupId = model.getGroupId() == null ? model.getParent().getGroupId() : model.getGroupId();
        String artifactId = model.getArtifactId() == null ? "" : model.getArtifactId();

        return groupId + ":" + artifactId;
    }

    private static VersionMoveDefinition getMoveDefinition(String coordinates, String currentVersion, boolean toRelease,
            VersionMoveDefinition[] definitions) {

        for (VersionMoveDefinition def : definitions) {
            if (def.getName().startsWith(coordinates)) {
                String versionCompare = toRelease ? def.getOldVersion() : def.getNewVersion();
                if (Objects.equals(versionCompare, currentVersion)) {
                    return def;
                }
            }
        }
        return null;
    }

    private static void moveVersion(String path, VersionMoveDefinition projectDefinition, boolean toRelease,
            VersionMoveDefinition[] propertyDefinitions, VersionMoveDefinition[] packageDefinitions)
            throws IOException, FileNotFoundException, XmlPullParserException {

        Model project = loadModel(path);
        logger.info(String.format("Handling project %s", getCoordinates(project)));

        String oldVersion = toRelease ? projectDefinition.getOldVersion() : projectDefinition.getNewVersion();
        String newVersion = toRelease ? projectDefinition.getNewVersion() : projectDefinition.getNewDevelopmentVersion();

        // If the project is at the old version, move it to the release version
        if (project.getVersion() != null && project.getVersion().equals(oldVersion)) {
            logger.info(String.format(" - Moving project version to %s", newVersion));
            project.setVersion(newVersion);
        }

        // If the parent at an old version, also move that
        if (project.getParent() != null) {
            VersionMoveDefinition def = getMoveDefinition(getCoordinates(project.getParent()), project.getParent().getVersion(), toRelease, packageDefinitions);
            String oldParentVersion = oldVersion;
            String newParentVersion = newVersion;
            if (def != null) {
                oldParentVersion = toRelease ? def.getOldVersion() : def.getNewVersion();
                newParentVersion = toRelease ? def.getNewVersion() : def.getNewDevelopmentVersion();
            }
            logger.info(String.format(" - Moving parent %s:%s version from %s to %s",
                    project.getParent().getGroupId(), project.getParent().getArtifactId(),
                    oldParentVersion, newParentVersion));
            project.getParent().setVersion(newParentVersion);
        }

        // Go through all properties
        for (Object propertyNameObj : project.getProperties().keySet()) {
            String propertyName = (String) propertyNameObj;
            String currentPropertyVersion = project.getProperties().getProperty(propertyName);
            String oldPropertyVersion = oldVersion;
            String newPropertyVersion = newVersion;
            VersionMoveDefinition def = getMoveDefinition(propertyName, currentPropertyVersion, toRelease, propertyDefinitions);
            if (def != null) {
                oldPropertyVersion = toRelease ? def.getOldVersion() : def.getNewVersion();
                newPropertyVersion = toRelease ? def.getNewVersion() : def.getNewDevelopmentVersion();
            }
            if (Objects.equals(currentPropertyVersion, oldPropertyVersion)) {
                project.getProperties().setProperty(propertyName, newPropertyVersion);
                logger.info(String.format(" - Moving property %s to version %s", propertyName, newPropertyVersion));
            }
        }

        // Go through any dependencies
        for (Dependency dep : project.getDependencies()) {
            String oldDependencyVersion = oldVersion;
            String newDependencyVersion = newVersion;
            VersionMoveDefinition def = getMoveDefinition(getCoordinates(dep), dep.getVersion(), toRelease, packageDefinitions);
            if (def != null) {
                oldDependencyVersion = toRelease ? def.getOldVersion() : def.getNewVersion();
                newDependencyVersion = toRelease ? def.getNewVersion() : def.getNewDevelopmentVersion();
            }
            if (Objects.equals(oldDependencyVersion, dep.getVersion())) {
                dep.setVersion(newDependencyVersion);
                logger.info(String.format(" - Moving dependency %s to version %s", getCoordinates(dep), newDependencyVersion));
            }
        }

        // Go through each module if it's an aggregator
        logger.info(String.format(" - Has %d modules.", project.getModules().size()));
        for (String moduleName : project.getModules()) {
            File moduleDirectory = new File(new File(path).getParentFile(), moduleName);
            File moduleFile = new File(moduleDirectory, "pom.xml");
            String modulePath = moduleFile.getCanonicalPath();

            if (moduleFile.exists()) {
                moveVersion(modulePath, projectDefinition, toRelease, propertyDefinitions, packageDefinitions);
            } else {
                logger.info(String.format(" - Could not find file for project %s. Skipping.", moduleFile.getAbsolutePath()));
            }
        }

        try (FileOutputStream outStream = new FileOutputStream(new File(path))) {
            MavenXpp3Writer writer = new MavenXpp3Writer();
            writer.write(outStream, project);
        }
    }

    private static Model loadModel(String path) throws FileNotFoundException, IOException, XmlPullParserException {
        Model project;
        try (FileInputStream inStream = new FileInputStream(path)) {
            MavenXpp3Reader reader = new MavenXpp3Reader();
            project = reader.read(inStream);
        }

        return project;
    }

    private static class VersionMoveDefinition {

        private final String name, oldVersion, newVersion, newDevelopmentVersion;

        public VersionMoveDefinition(String name, String oldVersion, String newVersion, String newDevelopmentVersion) {
            this.name = name;
            this.oldVersion = oldVersion;
            this.newVersion = newVersion;
            this.newDevelopmentVersion = newDevelopmentVersion;
        }

        public VersionMoveDefinition(String definition) {
            String[] parts = definition.split("=");
            name = parts[0].trim();
            String[] versions = parts[1].split(",");
            oldVersion = versions[0].trim();
            newVersion = versions[1].trim();
            newDevelopmentVersion = versions[2].trim();
        }

        public String getName() {
            return name;
        }

        public String getOldVersion() {
            return oldVersion;
        }

        public String getNewVersion() {
            return newVersion;
        }

        public String getNewDevelopmentVersion() {
            return newDevelopmentVersion;
        }
    }
}






